# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt

from services.publisher import orchestrator
from services.sendfiles import sendFile
# Create your views here.

@csrf_exempt
def first_server(request):
	if request.method == 'POST' and request.FILES['myfile'] :
		username = request.POST['username']
		password = request.POST['password']

		myfile = request.FILES['myfile']
		fs = FileSystemStorage()
		filename = fs.save(myfile.name, myfile)
		filepath = fs.path(filename)
		filesize = fs.size(filename)
			
		orchestrator(username,password,filename,filesize)
		sendFile(filename,filepath)

		return render(request, 'compress_tracker.html')

	return render(request, 'index.html')