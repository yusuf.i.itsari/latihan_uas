from decouple import config

import requests
import json
import pika

def callback(ch, method, properties, body):
	data = json.loads(body.decode('utf-8'))
	username = data.get('username','')
	password = data.get('password','')
	token_access = authenticate(username,password)

	if token_access:
		filename = data.get('filename','')
		filesize = data.get('filesize','')
		compressAnnounce(filename,filesize)
 		uploaded_file_url = fs.url(filename)


def authenticate(username,password):
	grant_type = config('GRANT_TYPE', default='password')
	client_id = config('CLIENT_ID', default='no client id')
	client_secret = config('CLIENT_SECRET', default='no client secret')
	payload = {
		'username': username,
		'password': password,
		'grant_type': grant_type,
		'client_id': client_id,
		'client_secret': client_secret
	}

	print payload

	url = 'http://172.22.0.2/oauth/token'
	r = requests.post(url, data=payload)

	data = r.json()
	token = data.get('token_access','')

	return token

def compressAnnounce(filename,filesize):
	# create exchange
	channel = make_channel()

	channel.exchange_declare(exchange='progress', exchange_type='direct')
	
	#lempar ke msgbroker bahwa proses zip sedang berlangsung
	message = json.dumps({'process':'compress file','filename':filename,'filesize':filesize})
	channel.basic_publish(exchange='progress',routing_key='compressor',body=message)

def make_channel():
	credentials = pika.PlainCredentials('1406578275','1406578275')
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
	channel = connection.channel()

	return channel

channel = make_channel()

channel.exchange_declare(exchange='progress', exchange_type='direct')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='progress', queue=queue_name, routing_key='orchestrator')

print(' [*] Waiting for logs. To exit press CTRL+C')

channel.basic_consume(callback, queue=queue_name, no_ack=True)

channel.start_consuming()