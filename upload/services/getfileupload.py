#!/usr/bin/env python
import pika
import sys
import json
import zipstream
import time

def callback(ch, method, properties, body):
	file = json.loads(body.decode('utf-8'))
	process = file.get('process','')
	print process
	
	filename = file['filename']
	if process == 'initiate-file':
		open(filename,'w') 
	else:
		data = file['data']
		with open(filename, 'a') as the_file:
			the_file.write(data)

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='progress', exchange_type='direct')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='progress', queue=queue_name, routing_key='upload')

print(' [*] Waiting for logs. To exit press CTRL+C')

channel.basic_consume(callback, queue=queue_name, no_ack=True)

channel.start_consuming()