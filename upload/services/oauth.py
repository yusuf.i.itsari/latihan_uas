from decouple import config

import requests
import json

def authenticate(username,password):
	grant_type = config('GRANT_TYPE', default='password')
	client_id = config('CLIENT_ID', default='no client id')
	client_secret = config('CLIENT_SECRET', default='no client secret')
	payload = {
		'username': username,
		'password': password,
		'grant_type': grant_type,
		'client_id': client_id,
		'client_secret': client_secret
	}

	print payload

	url = 'http://172.22.0.2/oauth/token'
	r = requests.post(url, data=payload)

	data = r.json()
	token = data.get('token_access','')

	return token