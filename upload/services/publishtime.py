import datetime
import pika
import time
import json

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='progress', exchange_type='direct')

while True:
	timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
	message = json.dumps({'action':'time','time':timestamp})
	channel.basic_publish(exchange='progress',routing_key='cots-uas',body=message)
	print(" [x] Sent %r" % message)
	time.sleep(1)