import os
import zipfile
import StringIO
import pika
import json

def orchestrator(username,password,filename,filesize):
	channel = make_channel()

	channel.exchange_declare(exchange='progress', exchange_type='direct')
	
	message = json.dumps({'process':'authorization','username':username,'password':password,'filename':filename,'filesize':filesize})
	channel.basic_publish(exchange='progress',routing_key='orchestrator',body=message)

def make_channel():
	credentials = pika.PlainCredentials('1406578275','1406578275')
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
	channel = connection.channel()

	return channel;