import os
import zipfile
import StringIO
import pika
import json

def sendFile(filename,filepath):
	channel = make_channel()

	channel.exchange_declare(exchange='progress', exchange_type='direct')
	
	file = open(filename,'rb')
	message = json.dumps({'process':'initiate-file','filename':filename})
	channel.basic_publish(exchange='progress',routing_key='upload_file',body=message)

	while True:
		data = file.read(1024)
		data = data.encode('base64')
		if not data:
			break;
		msg = {'filename':filename,'data':data}
		message = json.dumps(msg, ensure_ascii=False)
		channel.basic_publish(exchange='progress',routing_key='upload_file',body=message)

def make_channel():
	credentials = pika.PlainCredentials('1406578275','1406578275')
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
	channel = connection.channel()

	return channel;