#!/usr/bin/env python
import pika
import sys
import json
import zipstream
import time

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='progress', exchange_type='direct')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='progress', queue=queue_name, routing_key='compressor')

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):

	data = json.loads(body.decode('utf-8'))
	filename = data['filename']
	size = data['filesize']

	z = zipstream.ZipFile(mode='w',compression=zipstream.ZIP_DEFLATED)
	z.write(location)
	percent = 0
	total = 0
	time.sleep(2)

	with open(location + '.zip','wb') as f:
		for data in z:
			time.sleep(0.02)
			f.write(data)
			total += len(data)
			percent = float(total) / float(size) * 100
			print '[x] percentage ' + str(percent) + '%' + ' data ' + str(total) + ' size ' + str(size)
			message = json.dumps({'progress':percent})
			channel.basic_publish(exchange='progress',routing_key='cots-uas',body=message)

		message = json.dumps({'progress':'100'})
		channel.basic_publish(exchange='progress',routing_key='cots-uas',body=message)		
		print "compressed complete"

channel.basic_consume(callback, queue=queue_name, no_ack=True)

channel.start_consuming()